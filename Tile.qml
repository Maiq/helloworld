import QtQuick 2.0


Rectangle{
    
    property var fVal: 0
    property var hVal: 0
    color: "blue"
    property var totalVal: -1
    Text{
        id: f
        anchors.margins: 1
        anchors.left: parent.left
        anchors.top: parent.top
        text: "F:" + fVal
    }
    Text{
        id: h
        anchors.margins: 1
        anchors.right: parent.right
        anchors.top: parent.top
        text: "H:" + hVal
    }
    Text{
        id:total
        anchors.margins: 1
        anchors.bottom:parent.bottom
        text: totalVal
    }
}