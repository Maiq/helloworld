import qrc_assets
from PySide2.QtCore import Property, Signal, Slot, QObject
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType

class MyObj(QObject):
    def __init__(self, name: str=''):
        QObject.__init__(self)
        self.__name = name
     
    def get_name(self):
        return self.__name

    def set_name(self,name):
        self.__name = name
        self.name_change.emit()
    
    @Signal
    def name_change(self):
        pass
            
    name = Property(str, get_name, set_name, notify=name_change)
    
    @Slot()
    def invk(self):
        print(f'Invoked from {self.__name}')


class MyList(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.__lst = [ MyObj(str(2*i)) for i in range(11) ]
        
    def get(self):
        return self.__lst

    @Signal
    def value_change(self):
        pass

    lst = Property(list,get,notify=value_change)

    @Slot(int)
    def invk(self,idx):
        print(f'Invoke model index: {idx}')


class PythonObj(QObject):
    def __init__(self,name):
        QObject.__init__(self)
        self.__m1 = name
    
    def get(self):
        return self.__m1
    
    name = Property(str,get)


if __name__ == '__main__' : 
    app = QGuiApplication([])
    qmlRegisterType(MyObj,'MyObj',1,0,'MyObj')
    qmlRegisterType(MyList,'MyList',1,0,'MyList')
    engine = QQmlApplicationEngine('hello.qml')
    pObj = PythonObj("Object From Python")
    engine.rootContext().setContextProperty("PythonObj",pObj)
    app.exec_()
