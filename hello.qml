import QtQuick 2.3
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

import MyObj 1.0
import MyList 1.0

Window {
    id: root
    width: 600
    height: 400
    visible: true
    color: "red"
    
    property var foo: MyObj{
        name: "Hello World!"
        onName_change:{
            root.color = "green"
        }
    }
    
    property var bar: MyList{}

    ColumnLayout{   
        anchors.centerIn: parent
        Item{
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50
            Layout.fillHeight: true
            
            Text {
                id: myText
                anchors.centerIn: parent
                text: foo.name
            }
        }

        Rectangle{
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 30
            Layout.fillHeight: true
            radius: 5
            Text{
                anchors.centerIn: parent
                text: "ClickMe!"
            }
            MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        foo.name = "Oh, Hi Mark!"
                        root.height = 1000 //FIXME: Do something smarter to automatically change height
                        assetLoad.sourceComponent = assetMark
                }
            }
        }

        Grid{
            columns: 11
            spacing: 2
            Repeater{
                model: 44
                Tile{
                    totalVal: index
                    width: 50
                    height: 50
                    border.width:1
                    MouseArea{
                        hoverEnabled: true
                        anchors.fill: parent
                        onEntered:{
                            parent.color = "white"
                        }
                        onExited:{
                            parent.color = "blue"
                        }
                    }
                   
                }
            }
        }
        Row{
            spacing: 2
            Repeater{
                model: bar.lst
                delegate: Rectangle{
                    width: 25
                    height: 25
                    color: "cyan"
                    Text{
                        text: modelData.name
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            modelData.invk()
                            bar.invk(index)
                            console.log(PythonObj.name)
                        }
                    }
                }
            }
        }
        Loader{
            id: assetLoad
        }
       
    }

    Component{
        id: assetMark
        Image{
            source: "qrc:///assets/mark.png"
        }
    }
}
